resource "kubernetes_deployment" "django_app" {
  metadata {
    name = "${terraform.workspace}-${var.app_deployment_name}"
    namespace = var.namespace
    # Deployment label
    labels = {
        app = var.app_deployment_name
    }
  }

  spec {
    replicas = var.app_deployment_replicas

    selector {
    # ReplicaSet label set to match one of the pod's labels to manage
      match_labels = var.app_replicaset_pod_labels
    }

    template {
      metadata {
        # Pod's labels. Must match the one implemented under spec
        labels = var.app_replicaset_pod_labels

      }

      spec {
        container {
          image = var.app_deployment_image_name
          name  = var.app_deployment_container_name

          resources {
            limits = {
              cpu    = var.app_deployment_cpu_limit
              memory = var.app_deployment_memory_limit
            }
            requests = { # Kube scheduler choose the node according to those metrics
              cpu    = var.app_deployment_cpu_request
              memory = var.app_deployment_memory_request
            }
          }
        # Liveness probes are defined to detect if the application needs a restart or not
          liveness_probe {
            http_get {
              path = var.app_liveness_probe_path
              port = var.app_liveness_probe_port

              http_header {
                name  = "X-Liveness-Probe-Header"
                value = "Performed"
              }
            }

            initial_delay_seconds = var.app_liveness_probe_initial_delay_seconds # This tells the liveness probe to wait x seconds before
            # performing the first liveness probe
            period_seconds        = var.app_liveness_probe_period_seconds # This fields define the frequency of the liveness
            #probe check
          }
        }
      }
    }
  }
}