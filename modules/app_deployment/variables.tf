variable "app_deployment_name" {
  description = "App deployment name"
}

variable "namespace" {
  
}

variable "app_deployment_replicas" {
  description = "Define the application pods replica's number"

  validation {
    condition     = can(regex("^[0-9]+$", var.app_deployment_replicas))
    error_message = "Value must be a numeric string"
  }
}

variable "app_replicaset_pod_labels" {
  description = "Pod's label. Must be matched by the ReplicaSet selector"
}

variable "app_deployment_image_name" {
  description = "App image name and tag"
}

variable "app_deployment_container_name" {
  description = "App container name"
}

variable "app_deployment_cpu_limit" {
  description = "CPU limit"
}

variable "app_deployment_memory_limit" {
  description = "Memory limit"
}

variable "app_deployment_cpu_request" {
  description = "CPU request"
}

variable "app_deployment_memory_request" {
  description = "Memory request"
}

variable "app_liveness_probe_path" {
  description = "Liveness probe path"
}

variable "app_liveness_probe_port" {
  description = "Liveness probe port"
}

variable "app_liveness_probe_initial_delay_seconds" {
  description = "App liveness probes delay before doing the first check"
}

variable "app_liveness_probe_period_seconds" {
  description = "App liveness probe check frequency"
}