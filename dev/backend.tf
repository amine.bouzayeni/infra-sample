terraform {
  backend "kubernetes" {
    secret_suffix  = "dev-backend"
    config_path    = "~/.kube/config"
    config_context = "minikube"
  }
}
