terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.30.0" # Check corresponding Go Client Version
    }
  }
}

provider "kubernetes" {
  config_path    = var.config_path
  config_context = var.config_context
}