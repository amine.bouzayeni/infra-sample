module "dev_namespace" {
  source = "../modules/namespace"

  namespace = "${terraform.workspace}-${var.namespace}"

}

module "django_app_deployment" {
source = "../modules/app_deployment"  

depends_on = [
    module.dev_namespace
  ]

namespace = var.namespace

app_deployment_name = "django-sample-app"

app_deployment_replicas = var.replicas

app_replicaset_pod_labels = {

  app = "django-sample-app"

}

app_deployment_image_name = "mbouzayeni/django-app:v1.1"


app_deployment_container_name = "django-app"

app_deployment_cpu_limit = "500m"


app_deployment_memory_limit = "512Mi"


app_deployment_cpu_request = "250m"


app_deployment_memory_request = "256Mi"


app_liveness_probe_path = "/tasks"


app_liveness_probe_port = 8235

app_liveness_probe_initial_delay_seconds = 3

app_liveness_probe_period_seconds = 5


}

