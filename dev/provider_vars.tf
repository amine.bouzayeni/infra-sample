variable "config_path" {
  description = "kubernetes config file path"
  default = "~/.kube/config"
}

variable "config_context" {
  description = "The kubernetes context(cluster+access user)"
}